package ru.t1.dkozoriz.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IConnectionProvider {

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

}
