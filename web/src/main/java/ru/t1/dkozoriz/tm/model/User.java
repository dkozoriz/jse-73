package ru.t1.dkozoriz.tm.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "tm_web_user")
public class User {

    private static final long serialVersionUID = 1;

    @Getter
    @Setter
    @Id
    private String id = UUID.randomUUID().toString();

    @Getter
    @Setter
    private String login;

    @Getter
    @Setter
    private String passwordHash;

    @Getter
    @Setter
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

    @Getter
    @Setter
    private Boolean locked = false;

}