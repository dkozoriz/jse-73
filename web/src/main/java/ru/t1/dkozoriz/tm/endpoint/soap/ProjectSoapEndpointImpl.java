package ru.t1.dkozoriz.tm.endpoint.soap;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.dkozoriz.tm.dto.soap.*;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.service.ProjectService;
import ru.t1.dkozoriz.tm.util.UserUtil;


@Endpoint
@RequiredArgsConstructor
public class ProjectSoapEndpointImpl {

    private final ProjectService projectService;

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public static final String NAMESPACE = "http://tm.dkozoriz.t1.ru/dto/soap";

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        projectService.deleteAll(UserUtil.getUserId());
        return new ProjectClearResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) {
        final ProjectCountResponse response = new ProjectCountResponse();
        response.setCountProject(projectService.count(UserUtil.getUserId()));
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    private ProjectCreateResponse create(@RequestPayload final ProjectCreateRequest request) {
        final ProjectCreateResponse response = new ProjectCreateResponse();
        final Project project = new Project(request.getName());
        projectService.save(UserUtil.getUserId(), project);
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.deleteById(UserUtil.getUserId(), request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProjects(projectService.findAll(UserUtil.getUserId()));
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        final Project project = projectService.findById(UserUtil.getUserId(), request.getId());
        response.setProject(project);
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    private ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        projectService.update(UserUtil.getUserId(), request.getProject());
        return new ProjectSaveResponse();
    }

}