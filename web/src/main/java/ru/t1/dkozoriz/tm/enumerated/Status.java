package ru.t1.dkozoriz.tm.enumerated;


public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    public String getDisplayName() {
        return displayName;
    }

    Status(final String displayName) {
        this.displayName = displayName;
    }

}