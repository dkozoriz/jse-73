package ru.t1.dkozoriz.unit.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.marker.UnitCategory;
import ru.t1.dkozoriz.tm.configuration.DataConfiguration;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.repository.ProjectRepository;
import ru.t1.dkozoriz.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataConfiguration.class})
@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    private final Project project1 = new Project("Test Project 1", "description 1");

    private final Project project2 = new Project("Test Project 2", "description 2");

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
    }

    @After
    public void clean() {
        projectRepository.deleteByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdTest() {
        final String projectId = project1.getId();
        final Project project = projectRepository.findByUserIdAndId(UserUtil.getUserId(), projectId);
        Assert.assertEquals(project.getId(), project1.getId());
        Assert.assertEquals(project.getName(), project1.getName());
        Assert.assertEquals(project.getDescription(), project1.getDescription());
        Assert.assertEquals(project.getStatus(), project1.getStatus());
        Assert.assertEquals(project.getUserId(), project1.getUserId());
        Assert.assertEquals(project.getCreated(), project1.getCreated());
    }

    @Test
    public void countTest() {
        final long count = projectRepository.countByUserId(UserUtil.getUserId());
        Assert.assertEquals(count, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByIdTest() {
        final String projectId = project1.getId();
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), projectId);
        Assert.assertEquals(1, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteAllTest() {
        projectRepository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

}