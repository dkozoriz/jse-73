package ru.t1.dkozoriz.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dkozoriz.marker.UnitCategory;
import ru.t1.dkozoriz.tm.configuration.DataConfiguration;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.service.ProjectService;
import ru.t1.dkozoriz.tm.util.UserUtil;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataConfiguration.class})
@Category(UnitCategory.class)
public class ProjectServiceTest {

    private final Project project1 = new Project("Test Project 1", "description 1");

    private final Project project2 = new Project("Test Project 2", "description 2");

    @Autowired
    private ProjectService projectService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        projectService.save(UserUtil.getUserId(), project1);
        projectService.save(UserUtil.getUserId(), project2);
    }

    @After
    public void clean() {
        projectService.deleteAll(UserUtil.getUserId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdTest() {
        final Project project = projectService.findById(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project.getId(), project1.getId());
        Assert.assertEquals(project.getName(), project1.getName());
        Assert.assertEquals(project.getDescription(), project1.getDescription());
        Assert.assertEquals(project.getStatus(), project1.getStatus());
        Assert.assertEquals(project.getUserId(), project1.getUserId());
        Assert.assertEquals(project.getCreated(), project1.getCreated());
    }

    @Test
    public void addTest() {
        Project project = new Project("test_project", "test_description");
        project.setUserId(UserUtil.getUserId());
        projectService.save(UserUtil.getUserId(), project);
        Assert.assertEquals(3, projectService.count(UserUtil.getUserId()));
    }

    @Test
    public void updateTest() {
        Project project = new Project("test_project", "test_description");
        project.setUserId(UserUtil.getUserId());
        Assert.assertNull(projectService.update(UserUtil.getUserId(), project));
        projectService.save(UserUtil.getUserId(), project);
        project.setName("new_name");
        projectService.update(UserUtil.getUserId(), project);
        Assert.assertEquals("new_name", projectService.findById(UserUtil.getUserId(), project.getId()).getName());
    }

    @Test
    public void createTest() {
        projectService.create(UserUtil.getUserId(), "test_project", "test_description");
        Assert.assertEquals(3, projectService.count(UserUtil.getUserId()));
    }

    @Test
    public void countTest() {
        Assert.assertEquals(projectService.findAll(UserUtil.getUserId()).size(), projectService.count(UserUtil.getUserId()));
    }

    @Test
    public void deleteByIdTest() {
        projectService.deleteById(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(1, projectService.count(UserUtil.getUserId()));
    }

    @Test
    public void deleteAllTest() {
        projectService.deleteAll(UserUtil.getUserId());
        Assert.assertEquals(0, projectService.count(UserUtil.getUserId()));
    }

}