package ru.t1.dkozoriz.unit.endpoint;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.dkozoriz.marker.UnitCategory;
import ru.t1.dkozoriz.tm.configuration.DataConfiguration;
import ru.t1.dkozoriz.tm.configuration.SecurityWebApplicationInitializer;
import ru.t1.dkozoriz.tm.configuration.ServiceAuthenticationEntryPoint;
import ru.t1.dkozoriz.tm.configuration.WebApplicationConfiguration;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                WebApplicationConfiguration.class,
                DataConfiguration.class,
                ServiceAuthenticationEntryPoint.class,
                SecurityWebApplicationInitializer.class
        }
)
@Category(UnitCategory.class)
public class TaskEndpointTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    private static final String PROJECT_URL = "http://localhost:8081/api/task/";

    private final Task task1 = new Task("Test Task 1", "description 1");

    private final Task task2 = new Task("Test Task 2", "description 2");

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        add(task1);
        add(task2);
    }

    @After
    @SneakyThrows
    public void clean() {
        final String url = PROJECT_URL + "delete/all";
        mockMvc.perform(MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @SneakyThrows
    public void add(final Task task) {
        final String url = PROJECT_URL + "post";
        String taskJson = new ObjectMapper()
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(taskJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    public List<Task> findAll() {
        final String url = PROJECT_URL + "getAll";
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Task[].class));

    }

    @SneakyThrows
    public int count() {
        final String url = PROJECT_URL + "count";
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Integer.class);
    }

    @SneakyThrows
    public boolean findById(final String id) {
        final String url = PROJECT_URL + "get/" + id;
        return !mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString().equals("");
    }

    @SneakyThrows
    public void deleteById(final String id) {
        final String url = PROJECT_URL + "delete/" + id;
        mockMvc.perform(MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    public void deleteAll() {
        final String url = PROJECT_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getAllTest() {
        Assert.assertEquals(count(), findAll().size());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(findAll().size(), count());
    }

    @Test
    public void getByIdTest() {
        final String taskId = task1.getId();
        Assert.assertTrue(findById(taskId));
    }

    @Test
    public void addTest() {
        final int count = count();
        final Task task3 = new Task("Test Task 3", "description 3");
        task3.setUserId(UserUtil.getUserId());
        add(task3);
        Assert.assertEquals(count(), count + 1);
    }

    @Test
    public void deleteByIdTest() {
        final String taskId = task2.getId();
        deleteById(taskId);
        Assert.assertFalse(findById(taskId));
    }

    @Test
    public void deleteAllTest() {
        deleteAll();
        Assert.assertEquals(0, findAll().size());
    }

}