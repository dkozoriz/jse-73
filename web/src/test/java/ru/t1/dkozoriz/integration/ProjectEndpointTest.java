package ru.t1.dkozoriz.integration;

import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.dkozoriz.marker.IntegrationCategory;
import ru.t1.dkozoriz.tm.dto.Result;
import ru.t1.dkozoriz.tm.model.Project;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    private static String SESSION_ID;

    private static final String BASE_URL = "http://localhost:8081/api/project/";

    private final Project project1 = new Project("Test Project 1", "description 1");

    private final Project project2 = new Project("Test Project 2", "description 2");

    private static final HttpHeaders HEADER = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "http://localhost:8081/api/auth/login?username=test_user&password=test_user";
        final ResponseEntity<Result> response = restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        final HttpHeaders headersResponse = response.getHeaders();
        final List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        SESSION_ID = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(SESSION_ID);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + SESSION_ID));
        HEADER.setContentType(MediaType.APPLICATION_JSON);

    }

    @AfterClass
    public static void afterClass() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "http://localhost:8081/api/auth/logout";
        sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER));
    }

    private static ResponseEntity<List> sendRequestList(
            final String url,
            final HttpMethod method,
            final HttpEntity<List> httpEntity
    ) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<Project> sendRequest(
            final String url,
            final HttpMethod method,
            final HttpEntity<Project> httpEntity
    ) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Project.class);
    }

    private static ResponseEntity<Long> sendRequestNumber(
            final String url,
            final HttpMethod method,
            final HttpEntity<Project> httpEntity
    ) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Long.class);
    }

    @Before
    public void initTest() {
        final String url = BASE_URL + "post";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, HEADER));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project2, HEADER));
    }

    @After
    public void clean() {
        final String url = BASE_URL + "deleteAll";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADER));
    }

    @Test
    public void findAllTest() {
        final String url = BASE_URL + "getAll";
        Assert.assertEquals(2, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void findByIdTest() {
        final String getUrl = BASE_URL + "get/" + project1.getId();
        Assert.assertEquals(project1.getId(), sendRequest(getUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getId());
        Assert.assertEquals(project1.getName(), sendRequest(getUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getName());
        Assert.assertEquals(project1.getDescription(), sendRequest(getUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getDescription());
    }

    @Test
    public void addTest() {
        final String url = BASE_URL + "post";
        final Project project3 = new Project("Test Project 3", "description 3");
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project3, HEADER));
        final String getAllUrl = BASE_URL + "getAll";
        Assert.assertEquals(3, sendRequestList(getAllUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void countTest() {
        final String url = BASE_URL + "count";
        final String getUrl = BASE_URL + "getAll";
        final long count = sendRequestList(getUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size();
        System.out.println(sendRequestNumber(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody());
        Assert.assertEquals(count, sendRequestNumber(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().intValue());
    }

    @Test
    public void saveTest() {
        final String url = BASE_URL + "put";
        project2.setName("New project name");
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(project2, HEADER));
        final String getUrl = BASE_URL + "get/" + project2.getId();
        Assert.assertEquals("New project name", sendRequest(getUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().getName());
    }

    @Test
    public void deleteTest() {
        final String addUrl = BASE_URL + "post";
        final Project project4 = new Project("Test Project 4", "description 4");
        sendRequest(addUrl, HttpMethod.POST, new HttpEntity<>(project4, HEADER));
        final String deleteUrl = BASE_URL + "delete/" + project4.getId();
        sendRequest(deleteUrl, HttpMethod.DELETE, new HttpEntity<>(project4, HEADER));
        final String findUrl = BASE_URL + "getAll";
        Assert.assertEquals(2, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void deleteAllTest() {
        final String url = BASE_URL + "deleteAll";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(HEADER));
        final String findUrl = BASE_URL + "getAll";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

}