package ru.t1.dkozoriz.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.t1.dkozoriz.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "tm_web_role")
public class Role {

    private static final long serialVersionUID = 1;

    @Getter
    @Setter
    @Id
    private String id = UUID.randomUUID().toString();

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
    @Column(name = "roletype")
    private RoleType roleType = RoleType.USER;

    @Getter
    @Setter
    @ManyToOne
    private User user;

    @Override
    public String toString() {
        return roleType.name();
    }

}