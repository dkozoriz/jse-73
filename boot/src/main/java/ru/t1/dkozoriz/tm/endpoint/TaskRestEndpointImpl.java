package ru.t1.dkozoriz.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.api.TaskEndpoint;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.service.TaskService;
import ru.t1.dkozoriz.tm.util.UserUtil;

import javax.jws.WebMethod;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/task")
public class TaskRestEndpointImpl implements TaskEndpoint {

    private final TaskService taskService;

    @Override
    @WebMethod
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/getAll")
    public List<Task> getAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/count")
    public Long count() {
        return taskService.count(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/get/{id}")
    public Task get(
            @PathVariable("id") String id
    ) {
        return taskService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/post")
    public Task post(
            @RequestBody Task task
    ) {
        return taskService.save(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PreAuthorize("isAuthenticated()")
    @PutMapping("/put")
    public Task put(
            @RequestBody Task task
    ) {
        return taskService.update(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/delete/{id}")
    public void delete(
            @PathVariable("id") String id
    ) {
        taskService.deleteById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        taskService.deleteAll(UserUtil.getUserId());
    }

}