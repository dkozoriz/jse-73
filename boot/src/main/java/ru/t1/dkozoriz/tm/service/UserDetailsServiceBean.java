package ru.t1.dkozoriz.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.dto.CustomUser;
import ru.t1.dkozoriz.tm.enumerated.RoleType;
import ru.t1.dkozoriz.tm.model.Role;
import ru.t1.dkozoriz.tm.model.User;

import javax.annotation.PostConstruct;

@Service("UserDetailService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private UserService userService;

    @PostConstruct
    private void init() {
        userService.initUser("admin", "admin", RoleType.ADMINISTRATOR);
        userService.initUser("test", "test", RoleType.USER);
        userService.initUser("test_user", "test_user", RoleType.USER);
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        final User user = userService.findByLogin(userName);
        if (user == null) throw new UsernameNotFoundException(userName);
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(user.getRoles().stream()
                        .map(Role::toString)
                        .toArray(String[]::new))
                .build()).withUserId(user.getId());
    }

}