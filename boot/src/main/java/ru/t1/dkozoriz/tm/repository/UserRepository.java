package ru.t1.dkozoriz.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(String login);

}