package ru.t1.dkozoriz.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dkozoriz.tm.api.endpoint.IUserEndpoint;
import ru.t1.dkozoriz.tm.dto.model.UserDto;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.listener.AbstractListener;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @Autowired
    protected IUserEndpoint userEndpoint;

    @Autowired
    protected IAuthEndpoint authEndpoint;

    public AbstractUserListener(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    protected void showUser(@Nullable final UserDto user) {
        if (user == null) throw new EntityException("User");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    public String getArgument() {
        return null;
    }

}