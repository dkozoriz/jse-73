package ru.t1.dkozoriz.tm.api.service.dto.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;

import java.util.List;

public interface ITaskDtoService extends IBusinessDtoService<TaskDto> {

    @NotNull TaskDto create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull TaskDto create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable String projectId
    );

    @NotNull List<TaskDto> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    @NotNull
    TaskDto bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    );

    @NotNull
    TaskDto unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    );
}
