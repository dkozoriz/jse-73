package ru.t1.dkozoriz.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.service.ILoggerService;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

@Service
public final class LoggerService implements ILoggerService {

    @NotNull
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    private static final String CONFIG_FILE = "/src/main/resources/logger.properties";

    private static final String COMMANDS = "COMMANDS";

    private static final String COMMANDS_FILE = "commands.xml";

    private static final String ERRORS = "ERRORS";

    private static final String ERRORS_FILE = "errors.xml";

    private static final String MESSAGES = "MESSAGES";

    private static final String MESSAGES_FILE = "messages.xml";

    private static final LogManager MANAGER = LogManager.getLogManager();

    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    private static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);

    private static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);

    private static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);

    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    public static Logger getLoggerCommand() {
        return LOGGER_COMMAND;
    }

    public static Logger getLoggerError() {
        return LOGGER_ERROR;
    }

    public static Logger getLoggerMessage() {
        return LOGGER_MESSAGE;
    }


    private static void loadConfigFromFile() {
        try {
            @NotNull final Class<?> clazz = LoggerService.class;
            @NotNull final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE);
            MANAGER.readConfiguration(inputStream);
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    private static ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private static void registry(@NotNull final Logger logger, @NotNull final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty()) logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    @PostConstruct
    public void initJmsLogger() {
        @NotNull final String logs = propertyService.getApplicationLog();
        registry(LOGGER_COMMAND, logs + COMMANDS_FILE, false);
        registry(LOGGER_ERROR, logs + ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, logs + MESSAGES_FILE, true);
    }


}