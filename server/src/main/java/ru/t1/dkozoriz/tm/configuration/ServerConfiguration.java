package ru.t1.dkozoriz.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.dkozoriz.tm")
@EnableJpaRepositories("ru.t1.dkozoriz.tm.repository")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDBDriver());
        dataSource.setUrl(propertyService.getDBUrl());
        dataSource.setUsername(propertyService.getDBUsername());
        dataSource.setPassword(propertyService.getDBPassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.dkozoriz.tm.model", "ru.t1.dkozoriz.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getDBDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDBHmb2DDLAuto());
        properties.put(Environment.SHOW_SQL, propertyService.getDBShowSQL());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDBCacheSecondLevel());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getDBCacheFactory());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getDBCacheUseQuery());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getDBCacheUseMinPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getDBCacheRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDBCacheConfigFilePath());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}